package it.unisalento.pas.smartcitywastemanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

//@SpringBootTest
@DataMongoTest
class SmartCityWasteManagementApplicationTests {

    @Test
    void contextLoads() {
    }

}
