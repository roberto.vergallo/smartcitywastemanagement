# Deploying a dockerized application to AWS

## Identity management with AWS IAM

- we don't want to use our username and password to use AWS services from the CLI (I am not even sure if this is even possible!)
- as we only need access to S3 and Beanstalk, it makes sense to work with an account with limited permissions
- IAM allows us to manage access to the AWS services
- we will create a new user with the following settings:
    * account type: programmatic access
    * permissions: attach existing policy: AmazonS3FullAccess and  AdministratorAccess-AWSElasticBeanstalk
- the account details will be displayed only once
- go to *Settings > CI/CD > Variables > Add variable* and define the following unprotected variables:
    * AWS_ACCESS_KEY_ID
    * AWS_SECRET_ACCESS_KEY
    * AWS_DEFAULT_REGION
- AWS CLI will look for these variables and use them to authenticate

![AWS IAM](/docs/aws_iam.png "AWS IAM")

## Masking & protecting variables

- to define a variable, go to *Settings > CI/CD > Variables > Add variable*
- we typically store passwords or other secrets
- a variable has a key and a value
- it is a good practice to write the key in uppercase and to separate any words with underscores
- flags:
    * Protect variable: if enabled, the variable is not available in branches, apart from the default branch (main), which is a protected branch
    * Mask variable: if enabled, the variable value is never displayed in clear text in job logs

![example variables](/docs/variables.png "example variables")

## Environments

- every system where we deploy an application is an environment
- typical environments include test, staging & production
- GitLab offers support for environments
- we can define environments in *Deployments > Environments*

## Creating a new AWS Elastic Beanstalk application

- when creating an EB app, choose the *Docker* platform and deploy the sample app
- AWS EB will use two AWS services to run the application:
    * EC2 (virtual servers)
    * S3 (storage)
- to deploy a new version, go to the environment in EB and upload the file in templates called `Dockerrun.aws.json`

## Private registry authentication

- AWS EB requires authentication credentials to pull our Docker image
- GitLab allows us to create a Deploy Token that AWS can use to log to the registry
- to generate a Deploy Token, from your project, go to *Settings > Repository > Deploy tokens*.
- create a new Deploy Token with the following scopes:
    * read_repository
    * read_registry

![Deploy Token](/docs/deploy_token.png "Deploy Token")

## Deploying to AWS Elastic Beanstalk

- a new deployment to AWS EB happens in two steps
- step 1: we create a new application version with `aws elasticbeanstalk create-application-version`
- step 2: we update the environment with the new application version with `aws elasticbeanstalk update-environment`

## Post-deployment testing

- updating an EB environment does not happen instantly
- we will use `aws elasticbeanstalk wait` to know when the environment has been updated

```yml
    - aws elasticbeanstalk wait environment-updated --application-name "SmartCityWasteManagement" --version-label ${CI_COMMIT_SHA} --environment-name "prod"
```
