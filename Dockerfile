FROM openjdk:17-jdk-alpine

COPY build/libs/SmartCityWasteManagement-0.0.1-SNAPSHOT.jar /app/app.jar

EXPOSE 8080

WORKDIR /app

ENTRYPOINT ["java", "-jar", "app.jar"]

